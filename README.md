# delay-job-javascript

#### 介绍
不断轮训一个状态直到状态返回真，延迟执行一个任务函数

#### 软件架构
有时候你需要等待一个任务初始化完成之后才能执行下一个任务，典型的做法是用回调，但是回调的耦合较厉害。
所以：另一种办法是不断定时去轮训依赖的资源的状态(State)，如果可用则执行该延迟后的任务(Job)。


#### 安装教程

将此代码复制到你的前端项目中或者直接复制代码到你的工具类Javascript中。

#### 使用说明


```
let delayedResource = null;
function requestResource() {
  // 一个延迟的资源加载任务，延迟5秒
    setTimeout(()=> delayedResource = 'some data', 5000);
}

 // 请求资源
requestResource();

// 创建一个延迟任务：不断查询 delayedResource 是否可用，轮训周期默认50毫秒，等待超时10秒
createDelayJob(
    ()=> console.log('Hello delayed job! finished!'),
    ()=> delayedResource != null
    // 5000, // timeout Ms 超时时间
    // 50, // circle time Ms 轮训间隔时间
);

```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
