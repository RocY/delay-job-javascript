/*
 * Author: Rocbin, asboot#qq.com
 * Create: 2019-05-11 19:00
 */
function createDelayJob(jobFn, queryWaitFn, timeoutMs, circleMs) {
    const delay = circleMs || 50, timeout = timeoutMs || delay * 200;
    queryWaitFn = queryWaitFn || function () {
        return true
    };
    let t = 0;
    const delayJob = () => {
        setTimeout(() => {
            const state = queryWaitFn();
            if (state !== true) {
                t += delay;
                if (t < timeout) {
                    return delayJob();
                } else {
                    console.warn("Delay job timeout! job:%o", jobFn);
                    return;
                }
            }
            jobFn();
        }, delay);
    };
    delayJob();
}
